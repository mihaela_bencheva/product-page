$(document).ready(function () {
    $("tr:odd").css("background-color", "#28a745");
    $("tr:even").css("background-color", "#007bff");
    $("#promotionInput").keyup(function () {
        if ($("#promotionInput").val() >= 100 || $("#promotionInput").val() < 1) {
            $("#promotionInput").val("");
            alert("Въведи друго число!");
        }
    });
    $(".quantityShow").change(function () {
        var itemPrice = parseFloat($('.productNewPrice').text().replace(', ', '.'));
        var quantityNum = $(".quantityShow").val();
        var sum = quantityNum * itemPrice;
        sum = sum.toFixed(2).replace('.', ',');
        $(".endPrice").text("Обща сума: " + sum + " лв.");
    });
    $(".quantityShow").keyup(function () {
        if ($(".quantityShow").val() >= 100 || $(".quantityShow").val() < 1) {
            $(".quantityShow").val(1);
            alert("Въведи друго число!");
        }
    });
    $("#setPromotion").click(function () {
        var discountNum = $("#promotionInput").val();
        var discount = discountNum / 100;
        var oldPrice = $("#product_real_price").val();
        var newPrice = $("#product_real_price").val().replace(', ', '.') * (1 - discount);
        newPrice = newPrice.toFixed(2).replace('.', ', ');
        $(".productOldPrice").text(oldPrice + " лв.");
        $(".productNewPrice").text(newPrice + " лв.");
        $(".promotion").text(discountNum + "%");
        $(".productOldPrice").show();
        $(".promotion").show();
    }
    );
    $("#removePromotion").click(function () {
        var oldPrice = $(".productOldPrice").text();
        if (oldPrice == "") {
            alert("Съжаляваме, но тази функция не може да бъде изпълнена!");
        }
        else {
            $(".productNewPrice").text(oldPrice);
            $(".productOldPrice").text("");
            $(".promotion").text("");
            $(".productOldPrice").hide();
            $(".promotion").hide();
        }
    });
    $("#minusButton").click(function () {
        var quantityNum = $(".quantityShow").val();
        if (quantityNum > 1) {
            quantityNum = quantityNum - 1;
        }
        else {
            alert("Минимална стойност 1.");
        }
        $(".quantityShow").val(quantityNum);
        var itemPrice = parseFloat($('.productNewPrice').text().replace(', ', '.'));
        var quantityNum = $(".quantityShow").val();
        var sum = quantityNum * itemPrice;
        sum = sum.toFixed(2).replace('.', ',');
        $(".endPrice").text("Обща сума: " + sum + " лв.")
    });
    $("#plusButton").click(function () {
        var quantityNum = $(".quantityShow").val();
        if (quantityNum < 99) {
            quantityNum++;

        }
        else alert("Максимална стойност 99.");
        $(".quantityShow").val(quantityNum);
        var itemPrice = parseFloat($('.productNewPrice').text().replace(', ', '.'));
        var quantityNum = $(".quantityShow").val();
        var sum = quantityNum * itemPrice;
        sum = sum.toFixed(2).replace('.', ',');
        $(".endPrice").text("Обща сума: " + sum + " лв.");
    });
    $("#buyButton").click(function () {
        alert("Артикулът е добавен в кошницата.");
    });
    $("#googleInfo").click(function () {
        $("#infoModal").show(function(){
            $(".googleData").html("<b>Meta Title:</b> " + $('head meta[name="title"]').attr("content") + "<br>" + "<b>Meta Description:</b> "+ $('head meta[name="description"]').attr("content") + ": " + $(".productNewPrice").text() + "<br>" + "<b>Meta Keywords:</b> " + $('head meta[name="keywords"]').attr("content"));
        });
    });
    $("#infoClose").click(function () {
        $("#infoModal").hide();
    });
    $("#saunaImg").click(function(){
        $("#saunaModal").show();
    });
    $("#closeCursor").click(function(){
        $("#saunaModal").hide();
    });
});
